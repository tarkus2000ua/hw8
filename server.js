const express = require('express');
const cors = require('cors');
const path = require('path');
const app = express();
// const bcrypt = require('bcrypt');
// const { createProxyMiddleware } = require('http-proxy-middleware');
const PORT = process.env.PORT || 5000;


app.use(cors());
app.use(express.json());
// app.use('/api', createProxyMiddleware({ target: 'http://localhost:5000' }));

const routes = require('./routes/index');
routes(app);


app.use(express.static(path.join(__dirname, 'build')));

app.get('*', function (req, res) {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

app.listen(PORT);

exports.app = app;

