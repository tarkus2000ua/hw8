import {MESSAGES_API_ENDPOINT} from '../shared/config/config';

export const fetchMessagesService = (request) => {

  return fetch(MESSAGES_API_ENDPOINT)
    .then(response => {
      return response.json();
    })
    .then(json => {
      return json;
    });
};

export const addMessageService = (message) => {
  const parameters = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(message)
  };
  return fetch(MESSAGES_API_ENDPOINT, parameters)
    .then(response => {
      return response.json();
    })
    .then(json => {
      return json;
    });
};

export const updateMessageService = (id, body) => {
  const parameters = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body)
  };

  return fetch(MESSAGES_API_ENDPOINT+'/'+id, parameters)
    .then(response => {
      return response.json();
    })
    .then(json => {
      return json;
    });
};

export const toggleLikeService = (id, user) => {
  const parameters = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(user)
  };
  console.log(parameters);

  return fetch(MESSAGES_API_ENDPOINT+'/like/'+id, parameters)
    .then(response => {
      return response.json();
    })
    .then(json => {
      return json;
    });
};

export const deleteMessageService = (id) => {
  const parameters = {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json'
    },
  };

  return fetch(MESSAGES_API_ENDPOINT +'/'+ id, parameters)
    .then(response => {
      return response.json();
    })
    .then(json => {
      return json;
    });
};
