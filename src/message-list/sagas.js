import {
	call,
	put,
	takeEvery,
	all
} from 'redux-saga/effects';
import {
	FETCH_MESSAGES,
	ADD_MESSAGE,
	UPDATE_MESSAGE,
	DELETE_MESSAGE,
	TOGGLE_LIKE
} from "./actionTypes";
import {
	fetchMessagesService,
	addMessageService,
	updateMessageService,
	deleteMessageService,
	toggleLikeService
} from './messagesService';
import moment from 'moment';
import {
	showLoader,
	hideLoader
} from '../chat/actions';

export function* fetchMessages() {
	try {
		const messages = yield call(fetchMessagesService);
		yield put({
			type: 'FETCH_MESSAGES_SUCCESS',
			payload: messages
		});
	} catch (error) {
		console.log('fetchMessages error:', error.message)
	}
}

function* watchFetchMessages() {
	yield takeEvery(FETCH_MESSAGES, fetchMessages)
}

export function* addMessage(action) {
	const newMessage = action.payload;

	try {
		yield put(showLoader());
		yield call(addMessageService, newMessage);
		yield put({
			type: FETCH_MESSAGES
		});
		yield put(hideLoader())
	} catch (error) {
		console.log('createMessage error:', error.message);
	}
}

function* watchAddMessage() {
	yield takeEvery(ADD_MESSAGE, addMessage)
}

export function* updateMessage(action) {
	const id = action.payload.id;
	const text = action.payload.text;
	const editedAt = moment(Date.now()).format();
	const message = {
		text,
		editedAt
	};

	try {
		yield call(updateMessageService, id, message);
		yield put({
			type: FETCH_MESSAGES
		});
	} catch (error) {
		console.log('updateMessage error:', error.message);
	}
}

function* watchUpdateMessage() {
	yield takeEvery(UPDATE_MESSAGE, updateMessage)
}

export function* toggleLike(action) {
	const id = action.payload.id;
	const user = action.payload.user;

	try {
		yield call(toggleLikeService, id, user);
		yield put({
			type: FETCH_MESSAGES
		});
	} catch (error) {
		console.log('toggleLike error:', error.message);
	}
}

function* watchToggleLike() {
	yield takeEvery(TOGGLE_LIKE, toggleLike)
}

export function* deleteMessage(action) {
	try {
		yield call(deleteMessageService, action.payload);
		yield put({
			type: FETCH_MESSAGES
		})
	} catch (error) {
		console.log('deleteMessage Error:', error.message);
	}
}

function* watchDeleteMessage() {
	yield takeEvery(DELETE_MESSAGE, deleteMessage)
}


export default function* messagesSagas() {
	yield all([
		watchFetchMessages(),
		watchAddMessage(),
		watchUpdateMessage(),
		watchDeleteMessage(),
		watchToggleLike()
	])
}