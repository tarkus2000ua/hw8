import {
  ADD_MESSAGE,
  DELETE_MESSAGE,
  TOGGLE_LIKE,
  UPDATE_MESSAGE,
  CANCEL_EDIT_MESSAGE,
  EDIT_LAST_MESSAGE,
  FETCH_MESSAGES
} from './actionTypes';

export const addMessage = (text) => ({
  type: ADD_MESSAGE,
  payload: text
});

// export const editMessage = (id) => ({
//   type: EDIT_MESSAGE,
//   payload: id
// });

export const editLastMessage = () => ({
  type: EDIT_LAST_MESSAGE
});

export const cancelEditMessage = () => ({
  type: CANCEL_EDIT_MESSAGE
});

export const updateMessage = (id, text) => ({
  type: UPDATE_MESSAGE,
  payload: {
    id,
    text
  }
});

export const deleteMessage = (id) => ({
  type: DELETE_MESSAGE,
  payload: id
});

export const toggleLike = (id, user) => ({
  type: TOGGLE_LIKE,
  payload: {id,user}
});


export const fetchMessages = () => {
  return {
    type: FETCH_MESSAGES
  }
};