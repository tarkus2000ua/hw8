import {
    FETCH_MESSAGE_SUCCESS
} from "./actionTypes";

const initialState = {
    messageData: {
        id: '',
        text: '',
    }
};

const messageReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_MESSAGE_SUCCESS: {
            console.log(action.payload);
            const messageData = action.payload;
            return {
                ...state,
                messageData
            };
        }

        default:
            return state;
    }
}

export default messageReducer;