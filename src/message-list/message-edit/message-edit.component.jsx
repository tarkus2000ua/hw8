import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as actions from '../actions';
import { fetchMessage }  from './actions';

import './message-edit.styles.css';

class MessageEdit extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      text:''
    }
  }

  componentDidMount() {
    if (this.props.match.params.id) {
      this.props.fetchMessage(this.props.match.params.id);
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.messageData.id !== prevState.id && nextProps.match.params.id) {
      return {
        ...nextProps.messageData
      };
    } else {
      return null;
    }
  }

  onChange = (e) => {
    this.setState({ text: e.target.value });
  };

  onSubmit = (e) => {
    const { messageData, updateMessage } = this.props;
    e.preventDefault();
    updateMessage(messageData.id, this.state.text);
    this.setState({ text: '' });
    this.props.history.push('/chat');
  };

  onCancel = () => {
    this.setState({ text: '' });
    this.props.history.push('/chat');
  };

  render() {
    const { cancelEditMessage } = this.props;
    return (
      <div className="container">
        <div className="message-edit">
          <div className="message-edit-header"><div className="header-text">Edit message</div></div>
          <form className="message-edit" onSubmit={(e) => this.onSubmit(e)}>
            <div className="edit-wrapper">
              <textarea
                className="text"
                value={this.state.text}
                onChange={(e) => this.onChange(e)}
              ></textarea>
              <button type="submit" className="btn btn-save">
                Save
              </button>
              <button className="btn btn-cancel" onClick={cancelEditMessage}>
                Cancel
              </button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    messageData: state.message.messageData
  };
};

const mapDispatchToProps = {
  ...actions,
  fetchMessage
};

MessageEdit.propTypes = {
  message: PropTypes.shape({
    id: PropTypes.string.isRequired,
    likesCount: PropTypes.number.isRequired,
    userId: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired,
    createdAt: PropTypes.string,
    editedAt: PropTypes.string
  })
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageEdit);
