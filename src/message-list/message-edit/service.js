import {MESSAGES_API_ENDPOINT} from '../../shared/config/config';

export const fetchMessageService = (id) => {
  return fetch(MESSAGES_API_ENDPOINT+'/'+id)
    .then(response => {
      return response.json();
    })
    .then(json => {
      return json;
    });
};