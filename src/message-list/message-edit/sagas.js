import { call, put, takeEvery, all } from 'redux-saga/effects';
import { FETCH_MESSAGE, FETCH_MESSAGE_SUCCESS } from "./actionTypes";
import {fetchMessageService} from './service';

export function* fetchMessage(action) {
    try {
        const message = yield call(fetchMessageService, action.payload.id);
        console.log('hello');
        yield put({ type: FETCH_MESSAGE_SUCCESS, payload: message })
    } catch (error) {
        console.log('fetchMessage error:', error.message)
    }
}

function* watchFetchMessage() {
    yield takeEvery(FETCH_MESSAGE, fetchMessage)
}

export default function* messageEditSagas() {
    yield all([
        watchFetchMessage()
    ])
}