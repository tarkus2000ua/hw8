import {
  FETCH_MESSAGES_SUCCESS,
  EDIT_LAST_MESSAGE,
  CANCEL_EDIT_MESSAGE,
} from './actionTypes';

const initialState = {
  chatName: 'myChat',
  messageToEdit: null,
  scrollDown: true,
  messages: [],
  likes: [],

};

export const messagesReducer = (state = initialState, action) => {
  const {
    type,
    payload
  } = action;
  switch (type) {
    case FETCH_MESSAGES_SUCCESS: {
      return {...state, messages: payload};
    }

    case EDIT_LAST_MESSAGE: {
    const {
      currentUser,
      messages
    } = state;

    const curentUserMessages = messages.filter(message => message.userId === currentUser.id);
    let lastMessage = [...curentUserMessages]
      .map((message) => {
        return {
          id: message.id,
          date: new Date(message.createdAt).getTime()
        }
      })
      .sort((a, b) => b.date - a.date)[0];
    if (lastMessage) {
      const messageToEdit = messages.find((message) => message.id === lastMessage.id);
      return {
        ...state,
        messageToEdit: messageToEdit,
        scrollDown: false,
      };
    } else {
      return state;
    }
  }

  case CANCEL_EDIT_MESSAGE:
    return {
      ...state,
      messageToEdit: null,
      scrollDown: true
    }

  

  default:
  return state;
}
};

export default messagesReducer;