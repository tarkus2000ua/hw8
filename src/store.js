import {
  createStore,
  combineReducers,
  applyMiddleware
} from 'redux';
import {
  composeWithDevTools
} from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import messagesReducer from './message-list/reducer';
import messageReducer from './message-list/message-edit/reducer';
import loginReducer from './login/reducer';
import usersReducer from './user-list/reducer';
import userReducer from './user-list/user-edit/reducer';
import appReducer from './chat/reducer.js';
import rootSaga from './sagas/index';
import { createLogger } from 'redux-logger'

const reducers = {
  messages: messagesReducer,
  message: messageReducer,
  login: loginReducer,
  users: usersReducer,
  user: userReducer,
  app: appReducer 
};

const rootReducer = combineReducers({
  ...reducers
});

export default function configureStore() {
  const sagaMiddleware = createSagaMiddleware();
  const logger = createLogger();

  const middlewares = [sagaMiddleware];
  middlewares.push(logger);

  const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(...middlewares)));
  sagaMiddleware.run(rootSaga)

  return store;
}