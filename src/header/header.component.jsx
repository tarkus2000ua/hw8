import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import moment from 'moment';

import './header.styles.css';

const Header = ({ messages, chatName }) => {
  const info = getHeaderInfo(messages, chatName);
  return (
    <header className="header">
      <div className="container">
        <div className="chat-name">{info.chatName}</div>
        <div className="users-count">{info.usersCount} participants</div>
        <div className="messages-count">{info.messagesCount} messages</div>
        <div className="last-date">last message at: {info.lastDate}</div>
      </div>
    </header>
  );
};

const getHeaderInfo = (messages, chatName) => {
  const usersCount = new Set(messages.map((message) => message.userId)).size;
  const messagesCount = messages.length;
  let lastDate = [...messages]
    .map((message) => new Date(message.createdAt).getTime())
    .sort((a, b) => b - a)[0];
  lastDate = moment(lastDate).format('hh:mm DD.MM.YY');
  const info = {
    chatName,
    usersCount,
    messagesCount,
    lastDate
  };

  return info;
};

const mapStateToProps = (state) => {
  return {
    messages: state.messages.messages,
    chatName: state.messages.chatName
  };
};

Header.propTypes = {
  chatName: PropTypes.string.isRequired,
  messages: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      likesCount: PropTypes.number.isRequired,
      userId: PropTypes.string.isRequired,
      text: PropTypes.string.isRequired,
      user: PropTypes.string.isRequired,
      avatar: PropTypes.string,
      createdAt: PropTypes.string,
      editedAt: PropTypes.string
    })
  ).isRequired
};

export default connect(mapStateToProps)(Header);
