import moment from 'moment';

const formatDate = (date) => {
  if (isToday(date)) {
    return 'Today'
  } else if (isYesterday(date)) {
    return 'Yesterday'
  } else {
    return moment(date).format('MMMM Do')
  }
}

const isToday = (dateStr) => {
  const date = new Date(dateStr);
  const today = new Date()
  return date.getDate() === today.getDate() &&
    date.getMonth() === today.getMonth() &&
    date.getFullYear() === today.getFullYear()
}

const isYesterday = (dateStr) => {
  const date = new Date(dateStr);
  const yesterday = new Date(Date.now() - 864e5);
  return date.getDate() === yesterday.getDate() &&
    date.getMonth() === yesterday.getMonth() &&
    date.getFullYear() === yesterday.getFullYear()
}

export default formatDate;