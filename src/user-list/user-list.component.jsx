import React, { Component } from "react";
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import UserItem from './user-item/user-item.component';
import * as actions from './actions';
import PropTypes from 'prop-types';

import './user-list.styles.css';

class UserList extends Component {
	componentDidMount() {
		this.props.fetchUsers();
	}

	onEdit = (id) => {
		this.props.history.push(`/user/${id}`);
	}

	onDelete = (id) => {
		this.props.deleteUser(id);
	}

	onAdd = () => {
		this.props.history.push('/user');
	}

	render() {
		const {currentUser} = this.props;
    if (currentUser.role !== "Administrator"){
      return <Redirect to="/"/>
    }
		return (
			<div className="container">
  				<div className="btn-wrapper">
  					<button
  						className="btn"
  						onClick={this.onAdd}
  					>
  						Add user
  					</button>
  				</div>
          <div className="users-wrapper">
  					{
  						this.props.users?.map(user => {
  							return (
  								<UserItem
  									key={user.id}
  									id={user.id}
  									name={user.name}
  									surname={user.surname}
  									email={user.email}
  									onEdit={this.onEdit}
  									onDelete={this.onDelete}
  								/>
  							);
  						})
  					}
  				</div>
			</div>
		);
	}
}

UserList.propTypes = {
    userData: PropTypes.object
};

const mapStateToProps = (state) => {
	return {
		users: state.users,
		currentUser: state.login.currentUser
	}
};

const mapDispatchToProps = {
	...actions
};

export default connect(mapStateToProps, mapDispatchToProps)(UserList);