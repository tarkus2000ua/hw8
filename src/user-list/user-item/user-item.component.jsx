import React, { Component } from 'react';

import './user-item.styles.css';

export default class UserItem extends Component {
  render() {
    const { id, name, surname, email } = this.props;
    return (
      <div className="user-item">
        <div className="user-info">
          <span className="badge badge-name">
            {name} {surname}
          </span>
          <span className="badge badge-email">{email}</span>
        </div>
        <div className="btn-wrapper">
          <button
            className="btn btn-edit"
            onClick={(e) => this.props.onEdit(id)}
          >
            Edit
          </button>
          <button
            className="btn btn-delete"
            onClick={(e) => this.props.onDelete(id)}
          >
            Delete
          </button>
        </div>
      </div>
    );
  }
}
