import {
  USERS_API_ENDPOINT
} from '../shared/config/config';

export const fetchUsersService = (request) => {
  return fetch(USERS_API_ENDPOINT)
    .then(response => {
      return response.json();
    })
    .then(json => {
      return json;
    });
};

export const addUserService = (user) => {
  const parameters = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(user)
  };
  return fetch(USERS_API_ENDPOINT, parameters)
    .then(response => {
      return response.json();
    })
    .then(json => {
      return json;
    });
};

export const updateUserService = (id, body) => {
  const parameters = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body)
  };

  console.log(body);
  return fetch(USERS_API_ENDPOINT+'/'+id, parameters)
    .then(response => {
      return response.json();
    })
    .then(json => {
      return json;
    });
};

export const deleteUserService = (id) => {
  const parameters = {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json'
    },
    // body: JSON.stringify(request.user)
  };

  return fetch(USERS_API_ENDPOINT +'/'+ id, parameters)
    .then(response => {
      return response.json();
    })
    .then(json => {
      return json;
    });
};