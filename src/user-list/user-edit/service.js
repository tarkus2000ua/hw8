import {USERS_API_ENDPOINT} from '../../shared/config/config';

export const fetchUserService = (id) => {
  return fetch(USERS_API_ENDPOINT+'/'+id)
    .then(response => {
      return response.json();
    })
    .then(json => {
      return json;
    });
};