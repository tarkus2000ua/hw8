import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import * as actions from './actions';
import { addUser, updateUser } from '../actions';
import TextInput from '../../shared/inputs/text/TextInput';
import PasswordInput from '../../shared/inputs/password/PasswordInput';
import EmailInput from '../../shared/inputs/email/EmailInput';
import userFormConfig from '../../shared/config/userFormConfig';
import defaultUserConfig from '../../shared/config/defaultUserConfig';
import PropTypes from 'prop-types';
import './user-edit.styles.css';
const EMAIL = 'email';

class UserEdit extends Component {
  constructor(props) {
    super(props);
    this.state = this.getDefaultUserData();
    this.onCancel = this.onCancel.bind(this);
    this.onSave = this.onSave.bind(this);
    this.onChangeData = this.onChangeData.bind(this);
  }

  componentDidMount() {
    if (this.props.match.params.id) {
      this.props.fetchUser(this.props.match.params.id);
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.userData.id !== prevState.id && nextProps.match.params.id) {
      return {
        ...nextProps.userData
      };
    } else {
      return null;
    }
  }

  onCancel = () => {
    this.setState(this.getDefaultUserData());
    this.props.history.push('/users');
  };

  onSave = () => {
    if (this.state.id) {
      this.props.updateUser(this.state.id, this.state);
    } else {
      this.props.addUser(this.state);
    }
    this.setState(this.getDefaultUserData());
    this.props.history.push('/users');
  };

  onChangeData = (e, keyword) => {
    const value = e.target.value;
    this.setState({
      ...this.state,
      [keyword]: value
    });
  };

  getDefaultUserData = () => {
    return {
      ...defaultUserConfig
    };
  };

  getInput = (data, { label, type, keyword }, index) => {
    switch (type) {
      case 'text':
        return (
          <TextInput
            key={index}
            label={label}
            type={type}
            text={data[keyword]}
            keyword={keyword}
            onChange={this.onChangeData}
          />
        );
      case 'email':
        return (
          <EmailInput
            key={index}
            label={label}
            type={type}
            text={data[keyword]}
            keyword={keyword}
            ref={EMAIL}
            onChange={this.onChangeData}
          />
        );
      case 'password':
        return (
          <PasswordInput
            key={index}
            label={label}
            type={type}
            text={data[keyword]}
            keyword={keyword}
            onChange={this.onChangeData}
          />
        );
      default:
        return null;
    }
  };

  render() {
    const data = this.state;
    const {currentUser} = this.props;
    if (currentUser.role !== "Administrator"){
      return <Redirect to="/"/>
    }

    return (
      <div className="container">
        <div className="user-edit">
          <div className="content">
            <div className="header">
              <h5 className="title">Add user</h5>
            </div>
            <div className="body">
              {userFormConfig.map((item, index) =>
                this.getInput(data, item, index)
              )}
            </div>
            <div className="footer">
              <button className="btn btn-secondary" onClick={this.onCancel}>
                Cancel
              </button>
              <button className="btn btn-primary" onClick={this.onSave}>
                Save
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

UserEdit.propTypes = {
  userData: PropTypes.object
};

const mapStateToProps = (state) => {
  return {
    userData: state.user.userData,
    currentUser: state.login.currentUser
  };
};

const mapDispatchToProps = {
  ...actions,
  addUser,
  updateUser
};

export default connect(mapStateToProps, mapDispatchToProps)(UserEdit);
