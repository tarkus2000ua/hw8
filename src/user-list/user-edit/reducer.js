import { FETCH_USER_SUCCESS } from "./actionTypes";

const initialState = {
    userData: {
        id:'',
        name: '',
        surname: '',
        email: '',
        password: '',
        role:''
    }
};

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_USER_SUCCESS: {
            const { userData } = action.payload;
            return {
                ...state,
                userData
            };
        }

        default:
            return state;
    }
}

export default userReducer;
