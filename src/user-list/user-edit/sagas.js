import {
    call,
    put,
    takeEvery,
    all
} from 'redux-saga/effects';
import {
    FETCH_USER,
    FETCH_USER_SUCCESS
} from "./actionTypes";
import {
    fetchUserService
} from './service';

export function* fetchUser(action) {
    try {
        const user = yield call(fetchUserService, action.payload.id);
        yield put({
            type: FETCH_USER_SUCCESS,
            payload: {
                userData: user
            }
        })
    } catch (error) {
        console.log('fetchUsers error:', error.message)
    }
}

function* watchFetchUser() {
    yield takeEvery(FETCH_USER, fetchUser)
}

export default function* userEditSagas() {
    yield all([
        watchFetchUser()
    ])
}