import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import configureStore from './store';
import './index.css';
import Chat from './chat/Chat';
import Login from './login/login.component.jsx';
import UserList from './user-list/user-list.component.jsx';
import UserEdit from './user-list/user-edit/user-edit.component.jsx';
import MessageEdit from './message-list/message-edit/message-edit.component.jsx';

const store = configureStore();

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <Route exact path="/" component={Login}/>
      <Route exact path="/chat" component={Chat}/>
      <Route exact path="/users" component={UserList}/>
      <Route exact path="/user" component={UserEdit} />
			<Route exact path="/user/:id" component={UserEdit} />
			<Route exact path="/message/:id" component={MessageEdit} />
    </Router>
  </Provider>,
  document.getElementById('root')
);

