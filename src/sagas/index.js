import { all } from 'redux-saga/effects';
import loginSagas from '../login/sagas';
import messagesSagas from '../message-list/sagas';
import usersSagas from '../user-list/sagas';
import userEditSagas from '../user-list/user-edit/sagas';
import messageSagas from '../message-list/message-edit/sagas'; 

export default function* rootSaga() {
    yield all([
        loginSagas(),
        messagesSagas(),
        messageSagas(),
        usersSagas(),
        userEditSagas()
    ])
}