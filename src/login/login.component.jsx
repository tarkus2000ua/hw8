import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import {loginUser} from './actions';

import './login.styles.css';

class Login extends Component {

handleLogin = (event) => {
  event.preventDefault();

    const login = event.target.login.value;
    const password = event.target.password.value;

    const data = {
      login, password
    };

    this.props.dispatch(loginUser(data));
}

render(){
  const { loading, currentUser } = this.props;
  const { role } = currentUser;
  if (role === "User" || role === "Administrator"){
      localStorage.setItem('currentUser', JSON.stringify(currentUser));
  }
  if (role === "User"){
    return <Redirect to="/chat"/>
  }
  if (role === "Administrator"){
    return <Redirect to="/users"/>
  }
  return (
    <div id="login-page" className="login-page" onSubmit={this.handleLogin}>
    <form className="login-wrapper" name="login" id="login" >
      <div className="username-input-container">
        <input id="username-input" name="login" autoFocus placeholder="username" type="text"/>
      </div>
      <div className="password-input-container">
        <input id="password-input" name="password" placeholder="password" type="password"/>
      </div>
      <input  type="submit" id="submit-button" className="btn btn-submit" value="Login"/>
    </form>
    {loading && <div className="loader"> Loading... </div>}
  </div>
  )
}
}

const mapStateToProps = (state) => {
  return {
    loading: state.app.loading,
    currentUser: state.login.currentUser
  };
};


export default connect(mapStateToProps)(Login);