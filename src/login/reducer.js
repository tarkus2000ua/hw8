import * as types from './actionTypes';
const initialState = {
  currentUser: {
    id: null,
    name: null,
    avatar: null,
    role: null
  }
}

const loginReducer =(state = initialState, action) =>{
  const currentUser = action.response;

  switch(action.type) {
    case types.LOGIN_USER:{
      return { ...state};
    }
    case types.LOGIN_USER_SUCCESS:{
      return { ...state, currentUser};
    }
    case types.LOGIN_USER_ERROR:
      return { ...state, currentUser };
    default:
      return state;
  }
};

export default loginReducer;