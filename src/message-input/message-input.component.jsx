import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addMessage } from '../message-list/actions';
import moment from 'moment';
import { v4 as uuidv4 } from 'uuid';

import './message-input.component.css';

class MessageInput extends Component {
  constructor() {
    super();
    this.state = {
      text: ''
    };
  }

  onChange = (e) => {
    this.setState({ text: e.target.value });
  };

  onSubmit = (e) => {
    const { currentUser } = this.props;
    e.preventDefault();
    const message = {
      id: uuidv4(),
      userId: currentUser.id,
      avatar: currentUser.avatar,
      user: currentUser.name,
      text: this.state.text,
      editedAt: '',
      createdAt: moment(Date.now()).format(),
      likesCount: 0
    };
    this.props.addMessage(message);
    this.setState({ text: '' });
  };

  render() {
    return (
      <div className="container">
        <form className="message-input" onSubmit={(e) => this.onSubmit(e)}>
          <textarea
            className="text"
            placeholder="Message"
            onChange={(e) => this.onChange(e)}
            value={this.state.text}
          ></textarea>
          <button className="btn">Send</button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return { currentUser: state.login.currentUser };
};

const mapDispatchToProps = {
  addMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageInput);
