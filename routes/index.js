const userRoutes = require('./userRoutes');
const messageRoutes = require('./messageRoutes');

module.exports = (app) => {
    app.use('/users', userRoutes);
    app.use('/messages', messageRoutes);
  };