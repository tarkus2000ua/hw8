var express = require('express');
var router = express.Router();
const fs = require('fs');

let users = [];

fs.readFile('users.json', (err, data) => {
  if (err) throw err;
  users = JSON.parse(data);
});

// Get all users
router.get('/', (req, res) => {
  res.json(users)
})


// Get user 
router.get('/:id', (req, res) => {
  const result = users.find(user => user.id === req.params.id);
  if (result) {
    res.status(200).json(result);
  } else {
    res.status(400).json({
      success: false,
      msg: 'User not found'
    })
  }
});

// Post user
router.post('/', async (req, res) => {
  try {
    // const hashedPassword = await bcrypt.hash(req.body.password, 10)
    // const user = {...req.body, password: hashedPassword}
    const user = {
      ...req.body
    }
    users.push(user);
    writeUsersToFile(users);
    res.status(201).json({
      msg: "success"
    })
  } catch (error) {
    res.status(500).send()
    console.log(error);
  }
})

//         PUT /users/:id
router.put('/:id', async (req, res) => {
  try {
    const user = users.find(user => user.id === req.params.id);
    if (user) {
      user.login = req.body.login;
      user.name = req.body.name;
      user.surname = req.body.surname;
      user.password = req.body.password;
      user.role = req.body.role;
      user.email = req.body.email;
      writeUsersToFile(users);
      res.status(200).json(user);
    } else {
      res.status(400).json({
        success: false,
        msg: 'User not found'
      })
    }
  } catch (error) {
    console.log(error);
  }
});

//         DELETE /users/:id
router.delete('/:id', (req, res) => {
  const index = users.findIndex(user => user.id === req.params.id);
  if (index !== -1) {
    users.splice(index, 1);
    writeUsersToFile(users);
    res.status(200).json(users);
  } else {
    res.status(400).json({
      success: false,
      msg: 'User not found'
    })
  }
});

router.post('/login', async (req, res) => {
  const user = users.find(user => user.login === req.body.login)
  if (user === null) {
    return res.status(400).json({
      success: false,
      msg: 'User not found'
    })
  }
  try {
    // if (await bcrypt.compare(req.body.password, user.password)) {
    if (req.body.password === user.password) {
      res.status(200).json(user)
    } else {
      res.status(400).json({
        success: false,
        msg: 'User not found'
      })
    }
  } catch {
    res.status(500).send()
  }
})

const writeUsersToFile = (users) => {
  let data = JSON.stringify(users);
  fs.writeFileSync('users.json', data)
}

module.exports = router;