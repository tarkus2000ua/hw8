var express = require('express');
var router = express.Router();
const fs = require('fs');

let messages = [];
let likes = [];

fs.readFile('messages.json', (err, data) => {
  if (err) throw err;
  messages = JSON.parse(data);
});

fs.readFile('likes.json', (err, data) => {
  if (err) throw err;
  likes = JSON.parse(data);
});



// Get all messages
router.get('/', (req, res) => {
  const mappedMessages = messages.map(message => {
    const likesCount = likes.filter(like => like.id === message.id).length;
    return {
      ...message,
      likesCount: likesCount
    }
  })
  res.json(mappedMessages)
})


// Get message 
router.get('/:id', (req, res) => {
  const result = messages.find(message => message.id === req.params.id);
  if (result) {
    const likesCount = likes.filter(like => like.id === result.id).length;
    const message = {
      ...result,
      likesCount: likesCount
    }
    res.status(200).json(message);
  } else {
    res.status(400).json({
      success: false,
      msg: 'message not found'
    })
  }
});

// Post message
router.post('/', async (req, res) => {
  try {
    // const hashedPassword = await bcrypt.hash(req.body.password, 10)
    // const message = {...req.body, password: hashedPassword}
    const message = {
      ...req.body
    }
    messages.push(message);
    writeJsonToFile(messages, 'messages.json');
    res.status(201).json({
      msg: "success"
    })
  } catch (error) {
    res.status(500).send()
    console.log(error);
  }
})

//         PUT /messages/:id
router.put('/:id', async (req, res) => {
  try {
    const message = messages.find(message => message.id === req.params.id);
    if (message) {
      message.text = req.body.text;
      message.createdAt = req.body.createdAt;
      message.editedAt = req.body.editedAt;
      writeJsonToFile(messages, 'messages.json');
      res.status(200).json(message);
    } else {
      res.status(400).json({
        success: false,
        msg: 'message not found'
      })
    }
  } catch (error) {
    console.log(error);
  }
});

//         Like /like/:id
router.post('/like/:id', (req, res) => {

  console.log(req.body);
  const index = likes.findIndex(
    (like) => like.id === req.params.id && like.userId === req.body.Id
  );
  if (index !== -1) {
    likes.splice(index, 1);
    messages.find((message) => message.id === req.params.id).likesCount -= 1;
  } else {
    likes.push({
      id: req.params.id,
      userId: req.body.Id
    });
    messages.find((message) => message.id === req.params.id).likesCount += 1;
  }
  writeJsonToFile(likes, 'likes.json');
  writeJsonToFile(messages, 'messages.json');
  res.status(200).json({
    status: 'ok'
  });
});

//         DELETE /messages/:id
router.delete('/:id', (req, res) => {
  const index = messages.findIndex(message => message.id === req.params.id);
  if (index !== -1) {
    messages.splice(index, 1);
    writeJsonToFile(messages, 'messages.json');

    const likesList = likes.filter(
      (like) => like.id !== req.params.id
    );
    likes = likesList;
    writeJsonToFile(likes, 'likes.json');

    res.status(200).json(messages);
  } else {
    res.status(400).json({
      success: false,
      msg: 'message not found'
    })
  }
});

router.post('/login', async (req, res) => {
  const message = messages.find(message => message.login === req.body.login)
  if (message == null) {
    return res.status(400).json({
      success: false,
      msg: 'message not found'
    })
  }
  try {
    // if (await bcrypt.compare(req.body.password, message.password)) {
    if (req.body.password === message.password) {
      res.status(200).json({
        success: true,
        role: message.role
      })
    } else {
      res.status(400).json({
        success: false,
        msg: 'message not found'
      })
    }
  } catch {
    res.status(500).send()
  }
})

const writeJsonToFile = (object, file) => {
  let data = JSON.stringify(object);
  fs.writeFileSync(file, data)
}

module.exports = router;